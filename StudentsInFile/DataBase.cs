﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInFile
{
    class DataBase
    {
        public List<Student> GetStudents(int count)
        {
            List<Student> stList = new List<Student>();
            Random rnd = new Random();
            for (int i = 1; i < count; i++)
            {
                var st = new Student
                {
                    Name = $"Name{i}",
                    SurName = $"SurName{i}yan",
                    Age = 16 + rnd.Next(count)
                };
                stList.Add(st);
            }

            return stList;
        }

    }
}
