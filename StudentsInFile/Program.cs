﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInFile
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new DataBase();
            var students = db.GetStudents(10);


            string path = @"C:\Users\asatryan\Desktop\Aro_files\PROGRAMMING\C#\C#_TaskProjects\WriteListIntoFile\students.txt";

            using (FileStream fileCreating = File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes("");
                // Add some information to the file.
                fileCreating.Write(info, 0, info.Length);
            }

            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (var student in students)
                    tw.WriteLine(student);
            }
            
            Console.WriteLine("thanks");
            //StreamReader sr = new StreamReader(path);
            //StreamWriter sw = new StreamWriter(path);




            Console.ReadLine();
        }
    }
}
